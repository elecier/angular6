import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vuelos-main-component',
  templateUrl: './vuelos-main-component.component.html',
  styleUrls: ['./vuelos-main-component.component.css']
})
export class VuelosMainComponentComponent implements OnInit {

 id: any;

  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params => { this.id = params['id']; });
  }

  ngOnInit(): void {
  }

}
